﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeService.App.Models.ViewModel
{
    public class NotificationResult
    {
        public string EmployeeName { get; set; }
        public string TranType { get; set; }
    }
}
