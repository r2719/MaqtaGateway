﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeService.App.Models.ViewModel
{
    public class NotificationCountResult
    {
        public int Count { get; set; }
    }
}
