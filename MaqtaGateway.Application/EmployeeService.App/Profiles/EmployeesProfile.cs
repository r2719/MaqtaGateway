﻿using AutoMapper;
using EmployeeService.App.Dtos;
using EmployeeService.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeService.App.Profiles
{
    public class EmployeesProfile : Profile
    {
        public EmployeesProfile()
        {
            //source -> target
            CreateMap<Employee, EmployeeReadDto>();
            CreateMap<EmployeeCreateDto, Employee>();
        }
         
    }
}
