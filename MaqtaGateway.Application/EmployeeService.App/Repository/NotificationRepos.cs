﻿using EmployeeService.App.Data;
using EmployeeService.App.Models;
using EmployeeService.App.Models.ViewModel;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeService.App.Repository
{
    public class NotificationRepos : INotificationRepos
    {
        private readonly AppDbContext _context;
        private readonly IHubContext<BroadcastHub, IHubClient> _hubContext;
        public NotificationRepos(AppDbContext context, IHubContext<BroadcastHub, IHubClient> hubContext)
        {
            _context = context;
            _hubContext = hubContext;
        }

        public async Task DeleteNotifications()
        {
           await _context.Database.ExecuteSqlRawAsync("TRUNCATE TABLE Notification");
        }

        public async Task<NotificationCountResult> GetNotificationCount()
        {
            int count = await _context.Notifications.CountAsync();
            NotificationCountResult result = new NotificationCountResult
            {
                Count = count
            };
            return result;
        }

        public async Task<List<NotificationResult>> GetNotificationMessage()
        {
            var results = from message in _context.Notifications
                          orderby message.Id descending
                          select new NotificationResult
                          {
                              EmployeeName = message.EmployeeName,
                              TranType = message.TranType
                          };
            return await results.ToListAsync();
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }

        public async Task SaveChangesBroadCastMessageAsync()
        {
            await _hubContext.Clients.All.BroadcastMessage();
        }
    }
}
