﻿using EmployeeService.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeService.App.Repository
{
    public interface IEmployeeRepos
    {
        Task<bool> SaveChangesAsync();
        Task SaveChangesBroadCastMessageAsync();
        Task<IEnumerable<Employee>> GetAllEmployeeAsync();
        Task<Employee> GetEmployeeByIdAsync(int id);
        Task CreateEmployeeAsync(Employee employee);
        Task DeleteEmployeeAsync(int id);
        Task EditEmployeeAsync(int id, Employee employee);
    }
}
