﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeService.App.Repository
{
    public interface IHubClient
    {
        Task BroadcastMessage(string v, string v1);
        Task BroadcastMessage();
    }
}
