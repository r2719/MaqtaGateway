﻿using EmployeeService.App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeService.App.Repository
{
    public interface IUserRepos
    {
        Task<string> RegisterAsync(RegisterViewModel model);
        Task<AuthenticationViewModel> GetTokenAsync(TokenRequestViewModel model);
        Task<string> AddRoleAsync(AddRoleViewModel model);
    }
}
