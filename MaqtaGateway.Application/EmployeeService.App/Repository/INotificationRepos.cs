﻿using EmployeeService.App.Models;
using EmployeeService.App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeService.App.Repository
{
    public interface INotificationRepos
    {
        Task SaveChangesBroadCastMessageAsync();
        Task<List<NotificationResult>> GetNotificationMessage();
        Task<NotificationCountResult> GetNotificationCount();
        Task DeleteNotifications();
        Task<bool> SaveChangesAsync();
    }
}
