﻿using EmployeeService.App.Data;
using EmployeeService.App.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeService.App.Repository
{
    public class EmployeeRepos : IEmployeeRepos
    {
        private readonly AppDbContext _context;
        private readonly IHubContext<BroadcastHub, IHubClient> _hubContext;
        public EmployeeRepos(AppDbContext context, IHubContext<BroadcastHub, IHubClient> hubContext)
        {
            _context = context;
            _hubContext = hubContext;
        }

        public async Task CreateEmployeeAsync(Employee employee)
        {
            if(employee == null)
            {
                throw new ArgumentException(nameof(employee));
            }
            Notification notification = new Notification()
            {
                EmployeeName = employee.Name,
                TranType = "New Employee is added"
            };
            
            await _context.Employees.AddAsync(employee);
            await _context.Notifications.AddAsync(notification);
        }

        public async Task DeleteEmployeeAsync(int id)
        {
            var employee = await _context.Employees.FindAsync(id);
            if(employee == null)
            {
                throw new ArgumentException(nameof(employee));
            }
            Notification notification = new Notification()
            {
                EmployeeName = employee.Name,
                TranType = "This Employee is deleted"
            };
            _context.Employees.Remove(employee);
            _context.Notifications.Add(notification);
        }

        public async Task EditEmployeeAsync(int id, Employee employee)
        {
            _context.Entry(employee).State = EntityState.Modified;

            Notification notification = new Notification()
            {
                EmployeeName = employee.Name,
                TranType = "Edit"
            };
           await _context.Notifications.AddAsync(notification);
        }


        public async Task<IEnumerable<Employee>> GetAllEmployeeAsync()
        {
            return await _context.Employees.ToListAsync();
        }

        public async Task<Employee> GetEmployeeByIdAsync(int id)
        {
            return await _context.Employees.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<bool> SaveChangesAsync()
        {
            
            return ( await _context.SaveChangesAsync() >= 0);
        } 
        public async Task SaveChangesBroadCastMessageAsync()
        {
            await _hubContext.Clients.All.BroadcastMessage();
        }
    }
}
