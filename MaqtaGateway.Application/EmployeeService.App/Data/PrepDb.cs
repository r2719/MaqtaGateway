﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeService.App.Data
{
    public static class PrepDb
    {
        public static void PrepPopulation(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                SeedData(serviceScope.ServiceProvider.GetService<AppDbContext>());
            }
        }

        private static void SeedData(AppDbContext context)
        {
            if (!context.Employees.Any())
            {
                Console.WriteLine("--> Seeding Data...");

                context.Employees.AddRange(
                    new Models.Employee() { Name = "Zeeshan Ayyub", Designation = "Senior Software Engineer", Company = "Speridian Technology", City="Lahore", Address="B-608 Joher Town Lahore", Gender="Male" },
                    new Models.Employee() { Name = "Waqar Shaikh", Designation = "Senior Software Engineer", Company = "Speridian Technology", City="Lahore", Address="B-608 Joher Town Lahore", Gender="Male" },
                    new Models.Employee() { Name = "Sufian", Designation = "Senior Software Engineer", Company = "Speridian Technology", City="Lahore", Address="B-608 Joher Town Lahore", Gender="Male" }
                    );

                context.SaveChanges();
            }
            else
            {
                Console.WriteLine("--> We already have data");
            }
        }
    }
}
