﻿using EmployeeService.App.Models.ViewModel;
using EmployeeService.App.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeService.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepos _repository;

        public UserController(IUserRepos repository)
        {
            _repository = repository;
        }
        [HttpPost("register")]
        public async Task<ActionResult> RegisterAsync(RegisterViewModel model)
        {
            var result = await _repository.RegisterAsync(model);
            return Ok(result);
        }
        [HttpPost("token")]
        public async Task<IActionResult> GetTokenAsync(TokenRequestViewModel model)
        {
            var result = await _repository.GetTokenAsync(model);
            return Ok(result);
        }
        [HttpPost("addrole")]
        public async Task<IActionResult> AddRoleAsync(AddRoleViewModel model)
        {
            var result = await _repository.AddRoleAsync(model);
            return Ok(result);
        }
    }
}
