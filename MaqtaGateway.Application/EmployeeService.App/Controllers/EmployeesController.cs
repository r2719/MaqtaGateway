﻿using AutoMapper;
using EmployeeService.App.Dtos;
using EmployeeService.App.Models;
using EmployeeService.App.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeService.App.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(Roles = "Administrator")]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeRepos _repository;
        private readonly IMapper _mapper;
        public EmployeesController(IEmployeeRepos repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<EmployeeReadDto>>> GetEmployees()
        {
            try
            {
                var employeelist = await _repository.GetAllEmployeeAsync();

                return Ok(_mapper.Map<IEnumerable<EmployeeReadDto>>(employeelist));
            }catch(Exception ex)
            {
                throw;
            }
        }
        [HttpGet("{id}", Name = "GetEmployeeById")]
        public async Task<ActionResult<EmployeeCreateDto>> GetEmployeeById(int id)
        {
            try
            {
                var employee = await _repository.GetEmployeeByIdAsync(id);
                if (employee != null)
                    return Ok(_mapper.Map<EmployeeReadDto>(employee));

            }catch(Exception ex)
            {
                throw;
            }
            return NotFound();
        }
        [HttpPost]
        public async Task<ActionResult<EmployeeReadDto>> CreateEmployee(EmployeeCreateDto employeeCreateDto)
        {
            var employee = _mapper.Map<Employee>(employeeCreateDto);
            try
            {
                await _repository.CreateEmployeeAsync(employee);
                await _repository.SaveChangesAsync();
                await _repository.SaveChangesBroadCastMessageAsync();
            }
            catch(Exception ex)
            {
                throw;
            }
            var employeeReadDto = _mapper.Map<EmployeeReadDto>(employee);

            return CreatedAtRoute(nameof(GetEmployeeById), new { Id = employeeReadDto.Id }, employeeReadDto);
        }
        [HttpPut("{id}")]
        public async Task<ActionResult> EditEmployee(int id, Employee employee)
        {
            if (id != employee.Id)
                return BadRequest();
            await _repository.EditEmployeeAsync(id, employee);
            try
            {
                await _repository.SaveChangesAsync();
                await _repository.SaveChangesBroadCastMessageAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                    return NotFound();
            }

            return NoContent();
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteEmployee(int id)
        {
            try
            {
                await _repository.DeleteEmployeeAsync(id);
                await _repository.SaveChangesAsync();
                await _repository.SaveChangesBroadCastMessageAsync();

                return NoContent();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
        }
    }
}
