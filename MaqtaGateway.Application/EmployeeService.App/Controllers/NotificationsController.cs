﻿using EmployeeService.App.Models.ViewModel;
using EmployeeService.App.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeService.App.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class NotificationsController : ControllerBase
    {
        private readonly INotificationRepos _repository;

        public NotificationsController(INotificationRepos repository)
        {
            _repository = repository;
        }

        // GET: api/Notifications/notificationcount  
        [Route("notificationcount")]
        [HttpGet]
        public async Task<ActionResult<NotificationCountResult>> GetNotificationCount()
        {

            return Ok(await _repository.GetNotificationCount());

        }
        // GET: api/Notifications/notificationresult  
        [Route("notificationresult")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<NotificationResult>>> GetNotificationMessage()
        {
            return await _repository.GetNotificationMessage();
        }
        // DELETE: api/Notifications/deletenotifications  
        [HttpDelete]
        [Route("deletenotifications")]
        public async Task<ActionResult> DeleteNotifications()
        {
            await _repository.DeleteNotifications();
            await _repository.SaveChangesAsync();
            await _repository.SaveChangesBroadCastMessageAsync();

            return Ok(NoContent());
        }
    }
}
